class Exercise
  # Raymond Colebaugh

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    words = str.split
    # Match a 5+ char string, and optionally capture punctuation
    re = /\A\w{5,}(?<punct>[.!?])*\z/	

    # Iterate over the words to substitute.
    words.each_with_index do |word, i|
      if word =~ re
        words[i] = "marklar#{ $~[:punct] }"
        words[i].capitalize! if $~.to_s[0] =~ /[A-Z]/
      end
    end
    words.join(" ")
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    fibonacci = []
    # Build an array for the Fibonacci sequence up to n
    1.upto nth do |i|
      if i == 1 or i == 2
        fibonacci.push 1
      elsif i > 2
        # Since we're counting from 1, the last item is at i - 2
        last, secondToLast = fibonacci[i - 2], fibonacci[i - 3]
        fibonacci.push secondToLast + last
      end
    end

    # Select the evens and compute the sum
    evens = fibonacci.select {|i| i % 2 == 0 }
    evens.inject {|sum, i| sum += i }
  end

end
